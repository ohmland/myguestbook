let express = require('express');
let app = express();
let fs = require('fs');
let bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({ extended: true });
app.set('view engine', 'ejs');

app.get('/', readData);
app.post('/', urlencodedParser, writeData);

function writeData(req, res){
    let dateTime = require('node-datetime');
    let dt = dateTime.create();
    dt.offsetInHours(7);
    let formatted = dt.format('d/m/y H:M:S');

    fs.readFile('./lib/data.json', 'utf8', function readFileCallback(err, data){
        if (err){
            console.log(err);
        } else {
            obj = JSON.parse(data); //now it an object
            obj.db.push({id: obj.db.length+1, name:req.body.txtAuthor, message:req.body.txtMessage, date:formatted}); //add some data
            json = JSON.stringify(obj); //convert it back to json
            fs.writeFile('./lib/data.json', json, 'utf8', ()=>{
                // readData(req,res);
                let tagline = obj.db;
                res.render('index', {
                    tagline: tagline
                });
            }); // write it back
        }
    });

}

function readData(req, res){
    fs.readFile('./lib/data.json', 'utf8', function readFileCallback(err, data){
        if (err){
            console.log(err);
        } else {
            obj = JSON.parse(data); //now it an object
            let tagline = obj.db;
            res.render('index', {
                tagline: tagline
            });
        }
    });
}

app.listen(process.env.PORT || 3000);


// {
//     "db":[
//         {
//             "id":1,
//             "name":"neymar",
//             "message":"i will be world cup 2018 champion",
//             "date":"03/06/18 10:21:31"
//         }
//     ]
// }